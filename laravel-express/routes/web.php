<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PostController@index');



Route::get('/login',['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/auth/login', 'Auth\LoginController@login');
Route::get('/auth/logout', 'Auth\LoginController@logout');


Route::get('/home', ['as' => 'admin.index', 'uses' =>'PostsAdminController@index']);

Route::group(['prefix' => 'admin', 'middleware'=>'auth'], function (){

Route::get('/login', 'Auth\LoginController@showLoginForm');
Route::get('home', ['as' => 'admin.index', 'uses' =>'PostsAdminController@index']);
Route::get('create', ['as' => 'admin.create', 'uses' =>'PostsAdminController@create']);
Route::post('store', ['as' => 'admin.store', 'uses' =>'PostsAdminController@store']);
Route::get('edit/{id}', ['as' => 'admin.edit', 'uses' =>'PostsAdminController@edit']);
Route::put('update/{id}', ['as' => 'admin.update', 'uses' =>'PostsAdminController@update']);
Route::get('delete/{id}', ['as' => 'admin.delete', 'uses' =>'PostsAdminController@destroy']);

});


// Route::get('/auth/logout', function (){
// 	Auth::logout();
// });

// Route::get('/auth',function (){
	
// 	if(Auth::attempt(['email' => 'marcelojunin@gmail.com', 'password' => 123456]))
// 	{
// 		return "true";
// 	}
// 		return "falha";

// });