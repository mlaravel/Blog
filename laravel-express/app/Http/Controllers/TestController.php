<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index($nome)
    {
    	return view('test.index',['nome'=>$nome]);
    }

    public function notas()
    {
    	$notas = [
    		0 => 'Nota 19',
    		1 => 'Nota 18',
    		2 => 'Nota 17',
    		3 => 'Nota 16',
    		4 => 'Nota 15',
    		5 => 'Nota 14',
    		6 => 'Nota 13',
    		7 => 'Nota 12',
    	];
    	return view('test.notas', compact('notas'));
    }
}
