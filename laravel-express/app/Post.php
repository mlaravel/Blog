<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
    	'title',
    	'content',
    ];

    public function comments()
    {
    	return  $this->hasMany('App\Comment');
    }

    public  function tags()
    {
    	return $this->belongsToMany('App\Tag','posts_tags');
    }

    public function getTagListAttribute()
    //é obrigado ter o get...Attribute nesse metodo 
    {
        $tags = $this->tags()->pluck('name')->all();
        //metodo lists foi removido no laravel 5.3. Deves usar o pluck
        return implode(',', $tags);
    }
}
