1 - Instala o composer global require "laravel/installer".
2 - Instala o laravel 
	laravel new laravel-express
	
	/**USANDO TINKER**/
1 - php artisan tinker
2 - $post = new App\Post //Sempre que for criar um novo dá um new
	//No caso $post é a variavel que estou usando, e \Post é a model.
3 - $post->title = "Frist Post"; $post->content = "Content ...";
4 - $post->save();
	Persiste os dados
5 - App\Post::all();
	Lista os dados
6 - Clausula where: $post = App\Post::find(1); 
	O 1 é o id
7 - Para fazer uma alteração, basta dar um find, dps atribuir um novo valor.
	$post->title = "Change title";
	$post->save();
8 - Para salvar dados em massa:
	1º - Define os dados na model
	  protected $fillable = [
    	'title',
    	'content'
    ];
	2º - App\Post::create(['title' => 'fourth post', 'content'=>'Content fourth...']);

	/**USANDO FACTORY**/

1 - No ModelFactory.php coloca
$factory->define(App\Post::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'title' => $faker->sentence,
        'content' => $faker->paragraph
    ];
});
2 - php artisan tinker
3 - factory('App\Post')->make();
4 - factory('App\Post')->create();
	Persiste os dados
	Criar 5 registros
	factory('App\Post',5)->create();
	
	/**USANDO SEED**/

1 - php artisan make:seeder PostsTableSeeder
2 - Em PostsTableSeeder

    public function run()
    {
        Post::truncate();
		//Exclui todos os registros
	}

3 - Em DatabaseSeeder.php
	
	public function run()
    {
         $this->call(PostsTableSeeder::class);
    }
	
4 - No console
	php artisan db:seed

5 - Para excluir e criar, PostsTableSeeder.php

  public function run()
    {
        Post::truncate();

        factory('App\Post', 15)->create();
    }
	
/**CRIAR MODEL + MIGRATION MANY TO ONE**/

1 - php artisan make:model NameModel -m
2 - Vai na migrate criada e define os campos
	 public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')->references('id')->on('posts');
			//criando uma foreign key, referenciando o id da tabela de posts
            $table->text('comment');
            $table->string('name');
            $table->string('email');
            $table->timestamps();
        });
    }
3 - php artisan migrate

/**MONTANDO O RELACIONAMENTO NA MODEL POST**/

4 - Na model posts, define 

	public function comments()
    {
    	return  $this->hasMany('App\Comment');
    }
	//informa que o post pode ter vários comentarios
	
4 - Inserindo dados via tinker
	php artisan tinker
	$post = App\post::find(1);
	$comment = App\Comment::create(['post_id'=>1, 'comment'=>'Legal','name'=>'Marcelo', 'email'=>'marcelojunin2010@hotmail.com']);
	$post->comments (exibe os comentarios relacionados ao post, pois $comments já é um atributo de post

/**MONTANDO O RELACIONAMENTO NA MODEL COMMENTS**/

5 - Na model comment, define
	
	public function post()
    {
    	return $this->belongsTo('App\Post');
    }
	
6 - No tinker
	$comment = App\Comment::find(1);
	//A variavel $comment recebe o valor da consulta. Retorna onde o id do comentario for 1
	$comment->post
	//Retorna o post relacionado ao comentario

/** Many to Many **/	

7 - Uma mesma tag pode estar em varios posts e um post pode ter varias tags
	php artisan make:model Tag -m
8 - Tabela relacional
	php artisan make:migration create_tags_posts_table --create=posts_tags
	//--create=posts_tags define o nome da tabela
9 - tags_posts fica assim
	public function up()
    {
        Schema::create('posts_tags', function (Blueprint $table) {
            $table->integer('post_id');
            $table->foreign('post_id')->references('id')->on('posts');
            $table->integer('tag_id');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }
9 - Model post
	 public  function tags()
    {
    	return $this->belongsToMany('App\Tag','posts_tags');
    }
	
	//belongsToMany muitos para muitos
	//,'posts_tags' é para informar que essa tabela que fz o relacionamento
10 - Model  tags
	
   public function posts()
   {
   		return $this->belongsToMany('App\Post', 'posts_tags')
   }
//**CRIANDO UMA SEEDER PARA TRABALHAR COM A PERSISTENCIA DE DADOS**//
11 - php artisan make:seeder tagTableSeeder
12 - configura na factory
	$factory->define(App\Tag::class, function (Faker\Generator $faker) {
		static $password;

		return [
			'name' => $faker->word
			];
13 - IMPORTANTE P KRL
	QND FOR FZ UM DB:SEED NA TABL C RELACIONAMENTO
	TEM  Q ATIVAR E DESATIVAR A VERIFICAÇÃO DE INTEGRIDADE
	USA DB::statement('SET FOREIGN_KEY_CHECKS=0;'); E DB::statement('SET FOREIGN_KEY_CHECKS=1;'); 
		 DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        
         $this->call(PostsTableSeeder::class);
         $this->call(TagTableSeeder::class);
    
         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
//**ATACHAR RELACIONAMENTOS**//
14 - Entra no tinker
	App\Tag::all();
	//Lista tds as tags
	$post = App\Post::find(1);
	$post->tags()->attach(1);
	//Vai atachar o post q tem o id 1 com a tag com id 1
	$post->tags()->attach([2,4,5]);
	//Vai atachar o post q tem o id 1 com a tag com od ids 2, 4 e 5
	$post->tags()->detach(5);
	//Vai remover o id 5 do relacionamento