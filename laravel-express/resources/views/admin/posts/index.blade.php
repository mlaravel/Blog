

@extends('template')

@section('content')

	<h1>Blog Admin</h1>

	<a href="{{route('admin.create')}}" class="btn btn-success">Create</a>

	<table class="table">
		<tr>
			<th>ID</th>
			<th>Title</th>
			<th>Action</th>
		</tr>
		@foreach($posts as $post)
		<tr>
			<td>{{$post->id}}</td>
			<td>{{$post->title}}</td>
			<td> 
			<a href="{{route('admin.edit',['id'=>$post->id])}}" class="btn btn-default">Edit</a>
			<a href="{{route('admin.delete',['id'=>$post->id])}}" class="btn btn-danger">Delete</a>
			 </td>
		</tr>
		@endforeach
	</table>
		{!! $posts->render() !!}
@endsection