
@extends('template')

@section('content')

<ul class="alert">
		@foreach($errors->all() as $error)
			<li>{{$error}}</li>
		@endforeach
	</ul>

<form action="/auth/login" method="post">
	{!! csrf_field() !!}
	<div>
		<input type="email" name="email" value="{{old('email')}}" placeholder="Email.." class="form-control">
	</div>
	<br>
	<div class="form-group">
		<input type="password" name="password" id="password" placeholder="Password.." class="form-control">
	</div>
	
	<div class="form-group">
		<input type="checkbox" name="remember"> Remember Me
	</div>
	
	<div class="form-group">
		<button type="submit" class="btn btn-success">Login</button>
	</div>

</form>

@endsection