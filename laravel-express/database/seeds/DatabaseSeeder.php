<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::statement('SET FOREIGN_KEY_CHECKS=0;');

         factory('App\User')->create(

        [
            'name' => 'Marcelo',
            'email' => 'marcelojunin@gmail.com',
            'password' => bcrypt(123456),
            'remember_token' => str_random(10),
        ]

        );
        
         $this->call(PostsTableSeeder::class);
         $this->call(TagTableSeeder::class);
    
         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
